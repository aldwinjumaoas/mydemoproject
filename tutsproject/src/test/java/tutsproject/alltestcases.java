package tutsproject;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class alltestcases 
{
	
	
	public static void testcaseone(ExtentTest extenttest, String browser)
	{
		extenttest.log(Status.INFO, "Step 1. Launch Test URL in "+browser+" browser ");
		extenttest.log(Status.INFO, "Step 2. Validate URL ");
	}
	
	public static void testcasetwo(ExtentTest extenttest, String browser)
	{
		extenttest.log(Status.INFO, "Step 1. Launch Test URL in "+browser+" browser ");
		extenttest.log(Status.INFO, "Step 2. Validate URL ");
	}
	
	public static void testcasethree(ExtentTest extenttest, String browser)
	{
		extenttest.log(Status.INFO, "Step 1. Launch Test URL in "+browser+" browser ");
		extenttest.log(Status.INFO, "Step 2. Locate PACKAGE menu in the top menu navigation ");
		extenttest.log(Status.INFO, "Step 3. Validate Package text ");
	}
	
	public static void testcasefour(ExtentTest extenttest, String browser)
	{
		extenttest.log(Status.INFO, "Step 1. Launch Test URL in "+browser+" browser ");
		extenttest.log(Status.INFO, "Step 2. Locate HOME menu in the top menu navigation ");
		extenttest.log(Status.INFO, "Step 3. Validate Home text ");
	}
	
	public static void testcasefive(ExtentTest extenttest, String browser)
	{
		extenttest.log(Status.INFO, "Step 1. Launch Test URL in "+browser+" browser ");
		extenttest.log(Status.INFO, "Step 2. Locate BLOG menu in the top menu navigation ");
		extenttest.log(Status.INFO, "Step 3. Validate Blog text ");
	}
	
	public static void testcasesix(ExtentTest extenttest, String browser)
	{
		extenttest.log(Status.INFO, "Step 1. Launch Test URL in "+browser+" browser ");
		extenttest.log(Status.INFO, "Step 2. Locate SEARCH button ");
		extenttest.log(Status.INFO, "Step 3. Validate if SEARCH button is Active ");
	}
	
	public static void testcaseseven(ExtentTest extenttest, String browser)
	{
		extenttest.log(Status.INFO, "Step 1. Launch Test URL in "+browser+" browser ");
		extenttest.log(Status.INFO, "Step 2. Locate CONTACT US menu in the top menu navigation ");
		extenttest.log(Status.INFO, "Step 3. Validate if Contact us is Active ");
		extenttest.log(Status.INFO, "Step 3. If Active, validate if css style text-transform is UPPERCASE ");
	}
	
	public static void testcasesevena(ExtentTest extenttest, String browser)
	{
		extenttest.log(Status.INFO, "Step 1. Launch Test URL in "+browser+" browser ");
		extenttest.log(Status.INFO, "Step 2. Locate CONTACT US menu in the top menu navigation ");
		extenttest.log(Status.INFO, "Step 3. If Active, validate if css style text-transform is UPPERCASE ");
	}

}
